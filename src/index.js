import express from "express";
import { v4 } from "uuid";

const app = express();
app.use(express.json());
app.listen(3000, () => console.log('Running at "htpp://localhost:3000'));

const db = [];

const checkIfExists = (req, res, next) => {
  req.filter = db.filter((elem) => elem.cpf == req.params.cpf);
  req.error = { error: "invalid cpf - user is not registered" };
  next();
};

app.use(checkIfExists);

app.post("/users", (req, res) => {
  const id = v4();
  const data = { id: id, name: "", cpf: "", notes: [] };

  const { name, cpf } = req.body;

  data.name = name;
  data.cpf = cpf;

  db.push(data);
  res.status(201).json(data);
});

app.get("/users", (req, res) => {
  res.status(200).json(db);
});

app.patch("/users/:cpf", checkIfExists, (req, res) => {
  if (req.filter.length === 0) {
    res.status(400).json(req.error);
    return;
  }

  const id = req.filter[0]["id"];
  const index = db.findIndex((elem) => elem.id === id);

  db[index] = { ...db[index], ...req.body };

  res.status(200).json({ message: "User is updated", users: db });
});

app.delete("/users/:cpf", checkIfExists, (req, res) => {
  if (req.filter.length === 0) {
    res.status(400).json(req.error);
    return;
  }
  const id = req.filter[0]["id"];
  const index = db.findIndex((elem) => elem.id === id);

  db.splice(index, 1);

  res.status(200).json({
    message: "User is deleted",
    users: db,
  });
});

app.post("/users/:cpf/notes", checkIfExists, (req, res) => {
  if (req.filter.length === 0) {
    res.status(400).json(req.error);
    return;
  }

  const copy = JSON.parse(JSON.stringify(req.filter));

  const index = db.findIndex((elem) => elem.id === copy[0].id);

  const date = new Date();

  const idnote = v4();
  const { title, content } = req.body;
  const data = {
    id: idnote,
    created_at: date.toUTCString(),
    title: title,
    content: content,
  };

  db[index]["notes"] = [...db[index]["notes"], data];

  res
    .status(201)
    .json({ message: `${title} was added into ${copy[index].name}'s notes` });
});

app.patch("/users/:cpf/notes/:id", checkIfExists, (req, res) => {
  if (req.filter.length === 0) {
    res.status(400).json(req.error);
    return;
  }
  const id = req.filter[0]["id"];
  const index = db.findIndex((elem) => elem.id === id);

  const noteId = db[index]["notes"].filter((elem) => elem.id === req.params.id);
  const noteIndex = db[index]["notes"].findIndex(
    (elem) => elem.id === noteId[0]["id"]
  );

  if (noteId.length === 0) {
    res.status(400).json({ error: "Note not found" });
    return;
  }

  const date = new Date();

  const { title, content } = req.body;
  const data = {
    title: title,
    content: content,
    updated_at: date.toUTCString(),
  };

  db[index]["notes"][noteIndex] = { ...db[index]["notes"][noteIndex], ...data };

  res.status(201).json(db[index]["notes"][noteIndex]);
});

app.delete("/users/:cpf/notes/:id", checkIfExists, (req, res) => {
  if (req.filter.length === 0) {
    res.status(400).json(req.error);
    return;
  }
  const id = req.filter[0]["id"];
  const index = db.findIndex((elem) => elem.id === id);

  const noteId = db[index]["notes"].filter((elem) => elem.id === req.params.id);
  const noteIndex = db[index]["notes"].findIndex(
    (elem) => elem.id === noteId[0]["id"]
  );

  if (noteId.length === 0) {
    res.status(400).json({ error: "Note not found" });
    return;
  }

  db[index]["notes"].splice(noteIndex, 1);

  res.status(204).json([]);
});

app.get("/users/:cpf/notes", checkIfExists, (req, res) => {
  if (req.filter.length === 0) {
    res.status(400).json(req.error);
    return;
  }
  const id = req.filter[0]["id"];
  const index = db.findIndex((elem) => elem.id === id);

  res.status(200).json(db[index]["notes"]);
});
